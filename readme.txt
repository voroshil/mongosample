1. Start containers
2. docker exec -ti mongo1 mongo
2.1 > rs.initiate( { _id : "rs0", members: [{ _id: 0, host: "mongo1:27017" },{ _id: 1, host: "mongo2:27017" },{ _id: 2, host: "mongo3:27017" }]})
2.1 > rs.initiate( { _id : "rs0", members: [{ _id: 0, host: "192.168.99.100:27117" },{ _id: 1, host: "192.168.99.100:27217" },{ _id: 2, host: "192.168.99.100:27317" }]})
    > rs.conf()
    > rs.status()
PRIMARY> rs.add("host1:27017")
PRIMARY> rs.remove("host1:27017")

Java driver throw exception (ONLY ONE) if node is down during data read from it, but reconnect completes fine, no data loss
Something wrong when two node down simulteneously and remaining node is SECONDARY an no arbiter presents


sharding != replication

mapReduce:
map: "function(){emit(this.birthYear,1);}"
reduce: "function(key, values){return Array.sum(values);}"

db.products.update(
   { _id: 100 },
   { $set:
      {
        quantity: 500,
        details: { model: "14Q3", make: "xyz" },
        tags: [ "coats", "outerwear", "clothing" ]
      }
   }
)

$push - add to array
$each - update all elements from array

