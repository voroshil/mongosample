package ru.omsu.imit;

import static com.mongodb.client.model.Filters.eq;
import static org.junit.Assert.assertTrue;

import com.mongodb.client.*;
import com.mongodb.client.model.FindOneAndUpdateOptions;
import com.mongodb.client.result.UpdateResult;
import org.bson.Document;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    public static String CONNECT_STRING = "mongodb://192.168.99.100:27117,192.168.99.100:27217,192.168.99.100:27317/rs0?replicaSet=rs0&retryReads=true&maxIdleTimeMS=5000";
    /**
     * Rigorous Test :-)
     */
    @Test
    public void testInsertMany()
    {
        MongoClient mongoClient = MongoClients.create(CONNECT_STRING);
        MongoDatabase database = mongoClient.getDatabase("rs0");
        MongoCollection<Document> collection = database.getCollection("test1");
        System.out.println(collection);
        List<Document> documents = new ArrayList<>();
        for(int i=0; i<100; i++) {
            Document newDocument = new Document("i", i)
                    .append("firstName", String.format("Petr%d",i))
                    .append("lastName", String.format("Petrov%d",i))
                    .append("city", "Omsk")
                    .append("birthYear", 1972 + (i%10));
            documents.add(newDocument);
        }
        collection.insertMany(documents);

        //database.createCollection("test1");

        assertTrue( true );
    }
    @Test
    public void testInsertOne()
    {
        MongoClient mongoClient = MongoClients.create(CONNECT_STRING);
        MongoDatabase database = mongoClient.getDatabase("rs0");
        MongoCollection<Document> collection = database.getCollection("test1");
        System.out.println(collection);
        Document newDocument = new Document()
                .append("firstName", "Petr")
                .append("lastName", "Petrov")
                .append("city", "Omsk")
                .append("birthYear", 1972);
        collection.insertOne(newDocument);
    }
    @Test
    public void testLoopInfiniteForHostDownTest()
    {
        MongoClient mongoClient = MongoClients.create(CONNECT_STRING);
        MongoDatabase database = mongoClient.getDatabase("rs0");
        MongoCollection<Document> collection = database.getCollection("test1");
        int j=1;
        int err = 0;
        while(true) {
            for(int i=0;i<100; i++) {
                try {
                    collection.find(eq("i", i)).first();
                }catch(Exception e){
                    e.printStackTrace();
                    err++;
                }
            }
            System.out.println("ok " + j + " err "+err);
            j++;
        }
    }
    @Test
    public void testSimpleMapReduce(){
        MongoClient mongoClient = MongoClients.create(CONNECT_STRING);
        MongoDatabase database = mongoClient.getDatabase("rs0");
        MongoCollection<Document> collection = database.getCollection("test1");
        MapReduceIterable<Document> documents = collection.mapReduce("function(){emit(this.birthYear,1);}", "function(key, values){return Array.sum(values);}");
        for(Document d : documents) {
            System.out.println(d);
        }
    }

    @Test
    public void testUpdateDoc(){
        MongoClient mongoClient = MongoClients.create(CONNECT_STRING);
        MongoDatabase database = mongoClient.getDatabase("rs0");
        MongoCollection<Document> collection = database.getCollection("test1");
        int err=0;
        for(int i=0;i<100; i++) {
            try {
                Document d2 = new Document().append("$push", new Document().append("purchases",i%100-5%10));
                UpdateResult i1 = collection.updateMany(eq("i", i), d2);
                System.out.println(i1);
            }catch(Exception e){
                e.printStackTrace();
                err++;
            }
        }
        System.out.println(err);
    }

    @Test
    public void testQueryCollection(){
        MongoClient mongoClient = MongoClients.create(CONNECT_STRING);
        MongoDatabase database = mongoClient.getDatabase("rs0");
        MongoCollection<Document> collection = database.getCollection("test1");
        for(Document d : collection.find()){
            System.out.println(d);
        }
    }
}
